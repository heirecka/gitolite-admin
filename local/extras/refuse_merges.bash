#!/usr/bin/env bash

# From: Danny van Dyk, Ingmar Vanhassel
# Purpose:
#   Prevent merges, ensure linear history.
#   Committers should be using 'git pull --rebase'
#   to avoid creating merge commits.

oldrev=$1
newrev=$2

# man git-show; %P: parent hashes
while read line; do
    if [[ 1 != $(echo ${line} | wc -w) ]]; then
        echo "Commit refused: You have a merge commit, please use \`git rebase\` before committing." >&2
        exit 1
    fi
done < <(git-log --pretty=format:%P ${oldrev}..${newrev})


# vim: set sw=4 sts=4 et ft=sh :
